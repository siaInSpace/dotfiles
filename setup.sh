#/bin/bash

dir="$( cd "$(dirname "$BASH_SOURCE[0]" )" >/dev/null && pwd )"

echo "================================"
echo "Starting...."
for dotfile in $dir/dots/*;do
	echo "Linking ${dotfile}"
	ln -sf "${dotfile}" "${HOME}/.$(basename "$dotfile")" 
done
echo "Done!"
echo "================================"

